using UnityEngine;
using UnityEngine.UI;

public class FpsCounter : MonoBehaviour
{
	static FpsCounter instance;

	private const float UPDATE_INTERVAL = 0.5f;

	[Header("Local References")]
	public Text fpsText;

	float currentFps;
    float fpsSum;
    float avgFps;
    int fpsCounter;

	float timer;
	
	void Start()
	{
		if (instance == null)
		{
			instance = this;

			ResetLocalData();
			UpdateVisual();

			timer = UPDATE_INTERVAL;

			DontDestroyOnLoad(gameObject);
		}
		else
		{
			Destroy(gameObject);
		}
	}

    void Update()
    {
        currentFps = 1f / Time.deltaTime;
        fpsSum += currentFps;
        fpsCounter++;
        avgFps = fpsSum / fpsCounter;

		timer -= Time.deltaTime;
		if (timer <= 0)
		{
			UpdateVisual();
			ResetLocalData();
			timer = UPDATE_INTERVAL;
		}
	}

    private void ResetLocalData()
    {
        currentFps = 0;
        fpsSum = 0;
        avgFps = 0;
        fpsCounter = 0;
    }

	private void UpdateVisual()
	{
		fpsText.text = "FPS: " + avgFps.ToString("0");
	}
}
